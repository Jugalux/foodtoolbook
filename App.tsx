 // import 'react-native-gesture-handler'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import Dashboard from './screens/Dashboard'
import DailyInformations from "./screens/DailyInformations"
//import Test from './screens/test'


const Stack = createStackNavigator()

export default function App() {
    return ( 
        <NavigationContainer>
            <Stack.Navigator initialRouteName = 'dailyInformations' screenOptions = {{headerShown:false}}>
                <Stack.Screen name = "dailyInformations" component = { DailyInformations }/>
                <Stack.Screen name = "dashboard" component = { Dashboard }/>

            </Stack.Navigator> 
        </NavigationContainer>
    );
}